# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import sale
from . import purchase


def register():
    Pool.register(
        # invoice.InvoiceUpdateStart,
        # sale.SaleUpdateStart,
        # purchase.PurchaseUpdateStart,
        module='account_invoice_update', type_='model')
    Pool.register(
        # invoice.InvoiceUpdate,
        # sale.SaleUpdate,
        # purchase.PurchaseUpdate,
        module='account_invoice_update', type_='wizard')
